import media
import fresh_tomatoes

datas = []

new_movie = media.Movie("Am&eacute;lie",
                        """The film is a whimsical depiction of contemporary Parisian life, set in Montmartre. It tells the story of a shy waitress, played by Audrey Tautou, who decides to change the lives of those around her for the better, while struggling with her own isolation.""",
                        2001,
                        "FR",
                        "https://upload.wikimedia.org/wikipedia/en/5/53/Amelie_poster.jpg",
                        "https://www.youtube.com/watch?v=HUECWi5pX7o",
                        "Jean-Pierre Jeunet",
                        123)
datas.append(new_movie)

new_tvshow = media.TVShow("Skins",
                        """This series follows the lives of sixth form students Tony Stonem, Michelle Richardson, Sid Jenkins, Cassie Ainsworth, Chris Miles, Jal Fazer, Maxxie Oliver and Anwar Kharral.""",
                        2007,
                        "UK",
                        "https://upload.wikimedia.org/wikipedia/en/3/38/Skins_series_1_boxset.png",
                        "https://www.youtube.com/watch?v=MXqvk0RBL0M",
                        1,
                        9)

datas.append(new_tvshow)

new_movie = media.Movie("Mommy",
                        """The story concerns a mother with a sometimes-violent teenage son, struggling to control his behaviour in a hypothetical future in which parents have the legal option to commit troubled youth to public hospitals. The story continues Dolan's themes in mother-son relationships in his films, and is shot in an unconventional aspect ratio.""",
                        2014,
                        "CA",
                        "https://upload.wikimedia.org/wikipedia/en/d/d5/Mommy-by-xavier-dolan-cannes-poster.jpg",
                        "https://www.youtube.com/watch?v=Q9LVLCYvqSI",
                        "Xavier Dolan",
                        138)
datas.append(new_movie)

new_movie = media.Movie("Hunger Games",
                        """The story takes place in a dystopian post-apocalyptic future in the nation of Panem, where boys and girls between the ages of 12 and 18 must take part in the Hunger Games, a televised annual event in which the 'tributes' are required to fight to the death until there is only one survivor. Katniss Everdeen volunteers to take her younger sister's place. Joined by her district's male tribute, Peeta Mellark, Katniss travels to the Capitol to train for the Hunger Games under the guidance of former victor Haymitch Abernathy.""",
                        2012,
                        "US",
                        "https://upload.wikimedia.org/wikipedia/en/4/42/HungerGamesPoster.jpg",
                        "https://www.youtube.com/watch?v=PbA63a7H0bo",
                        "Gary Ross",
                        142)
datas.append(new_movie)

new_movie = media.Movie("Blue is the Warmest Colour",
                        """Ad&egrave;le, a French teenager who discovers desire and freedom when a blue-haired aspiring painter enters her life.
                        The film charts their relationship from Ad&egrave;le's high school years to her early adult life and career as a school teacher.""",
                        2013,
                        "FR",
                        "https://upload.wikimedia.org/wikipedia/en/3/3e/La_Vie_d%27Ad%C3%A8le_film_poster.png",
                        "https://www.youtube.com/watch?v=qOiug_u7Wns",
                        "Abdellatif Kechiche",
                        179)
datas.append(new_movie)

new_tvshow = media.TVShow("Skins",
                        """This series follows the lives of sixth form students Tony Stonem, Michelle Richardson, Sid Jenkins, Cassie Ainsworth, Chris Miles, Jal Fazer, Maxxie Oliver, Anwar Kharral and new character Lucy 'Sketch'.""",
                        2008,
                        "UK",
                        "https://upload.wikimedia.org/wikipedia/en/8/82/Skins_series_2_boxset.png",
                        "https://www.youtube.com/watch?v=GlenMMJye6Y",
                        2,
                        10)

datas.append(new_tvshow)

new_movie = media.Movie("Back to the future",
                        """Marty McFly is sent back in time to 1955, where he meets his future parents in high school and accidentally becomes his mother's romantic interest. The eccentric scientist Dr. Emmett 'Doc' Brown, Marty's friend who helps him repair the damage to history by helping Marty cause his parents to fall in love. Marty and Doc must also find a way to return Marty to 1985.""",
                        1985,
                        "US",
                        "https://upload.wikimedia.org/wikipedia/en/d/d2/Back_to_the_Future.jpg",
                        "https://www.youtube.com/watch?v=qvsgGtivCgs",
                        "Robert Zemeckis",
                        116)
datas.append(new_movie)

new_tvshow = media.TVShow("Shameless",
                        """The series depicts the dysfunctional family of Frank Gallagher, a single father of six children. While he spends his days drunk, his kids learn to take care of themselves. The first season mainly focuses on the ups and downs of Fiona Gallagher's new relationship with Steve Lishman.""",
                        2011,
                        "US",
                        "https://upload.wikimedia.org/wikipedia/en/3/34/Shameless_Season_1.jpg",
                        "https://www.youtube.com/watch?v=9tvkYS5cA58",
                        1,
                        12)

datas.append(new_tvshow)

new_movie = media.Movie("The Intouchables",
                        """Driss is driving Philippe's Maserati Quattroporte at high speed. They are soon chased by the police: when they are caught, Driss, unfazed, doubles his bet with Philippe, convinced they can get an escort.""",
                        2012,
                        "FR",
                        "https://upload.wikimedia.org/wikipedia/en/9/93/The_Intouchables.jpg",
                        "https://www.youtube.com/watch?v=34WIbmXkewU",
                        "Olivier Nakache",
                        112)
datas.append(new_movie)

new_tvshow = media.TVShow("Shameless",
                        """It's summertime in Chicago, and the Gallaghers are back and up to the same tricks.""",
                        2012,
                        "US",
                        "https://upload.wikimedia.org/wikipedia/en/b/b7/Shameless_Season_2.jpg",
                        "https://www.youtube.com/watch?v=4GE7fFDNGMA",
                        2,
                        12)

datas.append(new_tvshow)

new_tvshow = media.TVShow("Skins",
                        """This series sees the introduction of a new cast; it follows the lives of sixth form students Effy Stonem, Pandora Moon, Thomas Tomone, James Cook, Freddie McClair, JJ Jones, Naomi Campbell, and twin sisters Emily and Katie Fitch.""",
                        2009,
                        "UK",
                        "https://upload.wikimedia.org/wikipedia/en/d/d6/Skins_series_3_boxset.png",
                        "https://www.youtube.com/watch?v=Ho69_sCkwyI",
                        3,
                        10)

datas.append(new_tvshow)

# This function call uses list of movie instances as input to generate an HTML file
# and open it in the browser.
fresh_tomatoes.open_movies_page(datas)
