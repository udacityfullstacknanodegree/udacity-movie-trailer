function cleanModal() {
        $("#desc-video-picture").empty()
        $("#desc-video-title").empty()
        $("#desc-video-year").empty()
        $("#desc-video-country").empty()
        $("#desc-video-description").empty()
        $("#desc-video-director").empty()
        $("#desc-video-duration").empty()
        $("#desc-video-season").empty()
        $("#desc-video-episodes").empty()
    }

    $(document).ready(function(){
        $("#movieButton").click(function(){
            var $target = $('.movie'),
                $toggle = $(this);
            if ($toggle.hasClass("notSelected"))
                $toggle.removeClass("notSelected").addClass("isSelected")
            else
                $toggle.removeClass("isSelected").addClass("notSelected")
            $target.slideToggle("slow", function () {
            });
        });
        $("#tvShowButton").click(function(){
            var $target = $('.tvshow'),
                $toggle = $(this);

            if ($toggle.hasClass("notSelected"))
                $toggle.removeClass("notSelected").addClass("isSelected")
            else
                $toggle.removeClass("isSelected").addClass("notSelected")
            $target.slideToggle("slow", function () {
            });
        });
    });

    // Pause the video when the modal is closed
    $(document).on('click', '.hanging-close, .modal-backdrop, .modal', function (event) {
        // Remove the src so the player itself gets removed, as this is the only
        // reliable way to ensure the video stops playing in IE
        //$("#trailer-video-container").empty();
        $('#trailer-video').each(function(){
  this.contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*')
});
    });
    // Start playing the video whenever the trailer modal is opened
    $(document).on('click', '.video-tile', function (event) {
        var trailerYouTubeId = $(this).attr('data-trailer-youtube-id')
        var videoPicture = $(this).attr('data-video-picture')
        var videoTitle = $(this).attr('data-video-title')
        var videoYear = $(this).attr('data-video-year')
        var videoCountryFlag = $(this).attr('data-video-country-flag')
        var videoCountryName = $(this).attr('data-video-country-name')
        var videoDescription = $(this).attr('data-video-description')
        var videoDirector = $(this).attr('data-video-director')
        var videoDuration = $(this).attr('data-video-duration')
        var videoSeason = $(this).attr('data-video-season')
        var videoEpisodes = $(this).attr('data-video-episodes')
        var sourceUrl = 'http://www.youtube.com/embed/' + trailerYouTubeId + '?html5=1&enablejsapi=1';

        cleanModal()
        $("#desc-video-picture").html("<img src='" + videoPicture + "' alt='"+ videoTitle +" poster'>")
        $("#desc-video-title").html(videoTitle)
        if (videoYear != "None")
            $("#desc-video-year").html(videoYear)
        if (videoCountryFlag != 'None') {
            $("#desc-video-country").html("<img src='img/" + videoCountryFlag + ".png' alt='"+ videoCountryName +" flag'> " + videoCountryName)
        }
        $("#desc-video-description").html(videoDescription)
        if (videoDirector)
            $("#desc-video-director").html("Directed by <strong>" + videoDirector + "</strong>")
        if (videoDuration && videoDuration != "None")
            $("#desc-video-duration").html("Duration " + videoDuration + "min")
        if (videoSeason)
            $("#desc-video-season").html("Season " + videoSeason)
        if (videoEpisodes)
            $("#desc-video-episodes").html("No. of episodes " + videoEpisodes)

        $("#trailer-video-container").empty().append($("<iframe></iframe>", {
          'id': 'trailer-video',
          'type': 'text-html',
          'src': sourceUrl,
          'frameborder': 0
        }));
    });
