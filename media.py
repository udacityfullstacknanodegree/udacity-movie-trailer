import webbrowser
from datetime import datetime

class Video():
    """ Parent class of Movie and TVShow classes
        Class constants:
            COUNTRIES_NAME is a dictionary of country names with their abbreviations
    """
    COUNTRIES_NAME = {
        "CA":"Canada",
        "FR":"France",
        "UK":"United Kingdom",
        "US":"United States"
    }

    def __init__(self, title, description, year, country, poster_image, trailer_youtube):
        """ Args:
                title (str): Title of the video
                description (str): Description of the video
                year (int): Release year
                country (str): Abbreviation of the Country of origin
                poster_image (str): Link to the video poster
                trailer_youtube (str): Youtube link to the video trailer
        """
        assert (title), "title: Must not be empty"
        self.title = title
        self.description = description
        self.year = self.setYear(year)
        self.country = self.setCountry(country.upper())
        self.poster_image_url = self.setImage(poster_image)
        self.trailer_youtube_url = self.setTrailer(trailer_youtube)

    def setYear(self, year):
        """ This function checks the validity of the year ans return the value
            The year must be a positive number and not beyond the current year

            Args:
                year (int): Release year

            Returns:
                year, if valid
                None, if invalid
        """
        if not year or year < 0 or year > datetime.now().year:
            return None
        return year

    def setCountry(self, country):
        """ This function checks if country is found in the dictionary COUNTRIES_NAME
            and return the value

            Args:
                country (str): Abbreviation of the country of origin

            Returns:
                country, if found in the dictionary
                None, if not found
        """
        if not country in self.COUNTRIES_NAME:
            return None
        return country

    def getCountryName(self):
        """ This function returns the name of the country according to the attribute country

            Returns:
                country name, if the attribute country is set
                None, if the attribute country is not set
        """
        if self.country:
            return self.COUNTRIES_NAME[self.country]
        return None

    def setImage(self, image):
        """ This function check if image is empty to set a default picture and return the image

            Args:
                image (str): Link of the video poster

            Returns:
                image, if image is set
                default picture, if image is not set
        """
        if not image:
            return 'img/unknown.png'
        return image

    def setTrailer(self, trailer):
        """ This function check if trailer is empty and return the value

            Args:
                trailer (str): Link of the Youtube video

            Returns:
                trailer, if not empty
                None, if empty
        """
        if not trailer:
            return None
        return trailer

class Movie(Video):
    """ Movie class used to instantiate a Movie object """
    def __init__(self, title, description, year, country, poster_image, trailer_youtube, director, duration):
        """ Args:
                title (str): Title of the video
                description (str): Description of the video
                year (int): Release year
                country (str): Abbreviation of the Country of origin
                poster_image (str): Link to the video poster
                trailer_youtube (str): Youtube link to the video trailer
                director (str): Name of the movie director
                duration (int): Duration, in minutes, of the movie
        """
        Video.__init__(self, title, description, year, country, poster_image, trailer_youtube)
        assert director, "director: Must not be empty"
        self.director = director
        self.duration = self.setDuration(duration)

    def setDuration(self, duration):
        """ This function checks the validity of the duration and return the value
            To be valid, the duration must be a positive number, superior to 0

            Args:
                duration (int): Duration, in minutes, of the movie

            Returns:
                duration, if valid
                None, if not valid
        """
        if not duration or duration < 1:
            return None
        return duration

class TVShow(Video):
    """ TVShow class used to instantiate a TV Show object """
    def __init__(self, title, description, year, country, poster_image, trailer_youtube, no_season, total_episode):
        """ Args:
                title (str): Title of the video
                description (str): Description of the video
                year (int): Release year
                country (str): Abbreviation of the Country of origin
                poster_image (str): Link to the video poster
                trailer_youtube (str): Youtube link to the video trailer
                no_season (int): Numero of the season
                total_episode (int): Number total of episodes for the season
        """
        Video.__init__(self, title, description, year, country, poster_image, trailer_youtube)
        assert (no_season >= 0), "no_season: Season number must be a positive number"
        assert (total_episode > 0), "total_episode: Number of episodes must be a positive number"
        self.no_season = no_season
        self.total_episode = total_episode
