import webbrowser
import os
import re

MOVIE_CLASS = 'media.Movie'
TVSHOW_CLASS = 'media.TVShow'

page_head = '''<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UTrailers</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
  </head>
  <body>
  <div class="container">
    <!-- Trailer Video Modal -->
    <div class="modal" id="desc-video">
      <div class="modal-dialog">
        <div class="modal-content">
          <a href="#" class="hanging-close" data-dismiss="modal" aria-hidden="true">
            <img src="https://lh5.ggpht.com/v4-628SilF0HtHuHdu5EzxD7WRqOrrTIDi_MhEG6_qkNtUK5Wg7KPkofp_VJoF7RS2LhxwEFCO1ICHZlc-o_=s0#w=24&h=24" alt="Close modal"/>
          </a>

          <div class="row">
            <div class="col-xs-12 text-right"><span id="desc-video-country"></span></div>
          </div>
          <div class="row">
            <div class="col-xs-5" id="desc-video-picture">
            </div>
            <div class="col-sm-6">
                <h2><span id="desc-video-title">Title</span> <small><span id="desc-video-year">Year</span></small></h2>
                <p class='text-right'><span id="desc-video-director"></span><span id="desc-video-season"></span><br>
                    <small><span id="desc-video-duration"></span><span id="desc-video-episodes"></span></small>
                </p>
                <hr>
                <p><span id="desc-video-description"></span></p>
            </div>
          </div>
          <hr>
          <h3>Trailer</h3>
          <div class="scale-media" id="trailer-video-container">
          </div>
        </div>
      </div>
    </div>

    <div id="header">
        <h1>UTrailers</h1>
    </div>
    <div id="menu">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <p class="text-right">
                    <a href="#" id="movieButton" class="btn isSelected" role="button">Movies</a>
                    <a href="#" id="tvShowButton" class="btn isSelected" role="button">TV Shows</a>
                </p>
            </div>
        </div>
    </div>
    <div id="content">
        <div class="row text-center"> '''

page_foot = '''
        </div>
    </div>
   </div>
</body>
</html>
 '''

page_content = '''
<div class="col-xs-12 col-sm-6 col-md-4 video-tile {video_type_class}" data-trailer-youtube-id="{trailer_youtube_id}"
                                                    data-video-picture="{video_picture}"
                                                    data-video-title="{video_title}"
                                                    data-video-year="{video_year}"
                                                    data-video-country-flag="{video_country_flag}"
                                                    data-video-country-name="{video_country_name}"
                                                    data-video-description="{video_description}"
                                                    {video_details}
                                                    data-toggle="modal"
                                                    data-target="#desc-video">
    <div class="video">
        <img src="{video_picture}" alt="{video_title} poster">
        <h2><span>{video_title}</span></h2>
    </div>
</div>
'''

def create_page_content(videos):
    content = ''
    for video in videos:
        video_details = ''
        # Get the class of the video (Movie or TV Show)
        video_type_class = str(video.__class__).lower()
        video_type_class = video_type_class[video_type_class.find('.')+1:]
        # Extract the youtube ID from the url
        if video.trailer_youtube_url:
            youtube_id_match = re.search(r'(?<=v=)[^&#]+', video.trailer_youtube_url)
            youtube_id_match = youtube_id_match or re.search(r'(?<=be/)[^&#]+', video.trailer_youtube_url)
            trailer_youtube_id = youtube_id_match.group(0) if youtube_id_match else None
        else:
            trailer_youtube_id = None
        if str(video.__class__) == MOVIE_CLASS:
            video_details = 'data-video-director="'+ video.director +'" data-video-duration="'+ str(video.duration) +'"'
        elif str(video.__class__) == TVSHOW_CLASS:
            video_details = 'data-video-season="'+ str(video.no_season) +'" data-video-episodes="'+ str(video.total_episode) +'"'

        # Append the tile for the video with its content filled in
        content += page_content.format(
            video_title=video.title,
            video_picture=video.poster_image_url,
            video_year=video.year,
            video_country_flag=video.country,
            video_country_name=video.getCountryName(),
            video_description=video.description,
            trailer_youtube_id=trailer_youtube_id,
            video_details=video_details,
            video_type_class=video_type_class
        )

    return content

def open_movies_page(videos):
  # Create or overwrite the output file
  output_file = open('dist/index.html', 'w')

  content = create_page_content(videos)

  # Output the file
  output_file.write(page_head + content + page_foot)
  output_file.close()

  # open the output file in the browser
  #url = os.path.abspath(output_file.name)
  #webbrowser.open('file://' + url, new=2) # open in a new tab, if possible
